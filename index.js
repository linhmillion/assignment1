
// The Bank part begin,set some global values.

let currentBalance = 200;
let fullName = "Joe Smith";
let loanTimes = 0;
let currentPay = 100;
let loanNumber = 0;
let gotALoan = false;
// display first balance with exchange function in browser.
document.getElementById("balance").innerHTML = currencyExchange(currentBalance);
document.getElementById("fullName").innerHTML = fullName;
// fetch element from html to use in js
const balanceElement = document.getElementById("balance");
const getLoanElement = document.getElementById("getLoan");

const repayButtonElement = document.getElementById("repay-btn");
repayButtonElement.style.display = "none";


function currencyExchange(number) {
  return number = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number);
}
//Add prompt function

function promptFunction() {
  userInput = prompt("Enter amount you want to loan:", "");
  loanNumber = loanNumber + userInput;
  if (userInput != null && userInput !="") {
    if ( loanNumber != 0 ) {
      alert("You can't have more than one loan");
      return false;
    } else  if (userInput > maximumLoanAllow) {
      alert("You can loan more 2 time of your current balance");
      return false;
    } else if (userInput < 0) {
      alert("I need to be a positive number");
      return false;
    } else {
      document.getElementById("LoanAmount").innerHTML = "You loaned " + currencyExchange(userInput);
        function addCurrent(loanNumber) {
          return currentBalance += parseInt(loanNumber);
        }
        addCurrent(userInput);
        return true
      }
    } else {
      return false;
    }
  }


//2.4  Once you have a loan, a new button labeled 'Repay Loan' show.
const repayButtonListener = () => {
  repayButtonElement.style.display = "none";
  if(currentPay >= loanNumber) {
  currentPay = currentPay - loanNumber;
  loanNumber = 0;
  document.getElementById("LoanAmount").innerHTML = "You loaned " + currencyExchange(loanNumber);
  addPayToBalance();
  document.getElementById("pay").innerHTML = currencyExchange(currentPay);
  repayButtonElement.style.display = "none";
  } else {
  loanNumber = loanNumber - currentPay;
  currentPay = 0;
  document.getElementById("LoanAmount").innerHTML = "You loaned " + currencyExchange(loanNumber);
  document.getElementById("pay").innerHTML = currencyExchange(currentPay);
  repayButtonElement.style.display = "none";
  }
}
// repayButtonElement.addEventListener("click",repayButtonListener);
//maximum amount that you can loan from the bank.
let maximumLoanAllow = double(currentBalance);

// add function that calculate and display new balance when use eventlistener
const getALoan = () => {
    addNewCurrent();
    document.getElementById("balance").innerHTML = currencyExchange(currentBalance);
    repayButtonElement.addEventListener("click",repayButtonListener);
    if(loanNumber > 0 && loanTimes === 1) {
      repayButtonElement.style.display ="block";
    } else {
      repayButtonElement.style.display ="none";
    }
  }
// Add eventlistener when they click the loan buttons
getLoanElement.addEventListener("click",getALoan);

// to check if they already have a loan.
function addNewCurrent() {
  gotALoan = promptFunction();
  if (gotALoan === true) {
    return loanTimes = 1;
  }
}
function double(num) {return num * 2};

function tenPercentOfANumber(x) {return x * 0.1};
function minusTenPercent(x) {
  let y = x - (x * 0.1);
  return y
  };

// Works functions begin

const payElement = document.getElementById("pay");
const bankButtonElement = document.getElementById("bank-btn");
const workButtonElement = document.getElementById("work-btn");
document.getElementById("pay").innerHTML = currencyExchange(currentPay);



function addPayToBalance() {
  currentBalance += currentPay;
  return currentBalance
}
//functions minus the 10% of currentPay to Loan number



const bankMoneyToYourAccount = () => {
  if (gotALoan == true && loanNumber > 0) {
    loanNumber = loanNumber - tenPercentOfANumber(currentPay);
    document.getElementById("LoanAmount").innerHTML = "You loaned " + currencyExchange(loanNumber);
    currentPay = minusTenPercent(currentPay);
    addPayToBalance();
  } else {
    addPayToBalance();
  }
  document.getElementById("balance").innerHTML = currencyExchange(currentBalance);
  currentPay = 0;
  document.getElementById("pay").innerHTML = currencyExchange(currentPay);
}
bankButtonElement.addEventListener("click", bankMoneyToYourAccount);



// work buttons
const addMoneyToYourPay = () => {
  currentPay += 100;
  document.getElementById("pay").innerHTML = currencyExchange(currentPay);
}
workButtonElement.addEventListener("click", addMoneyToYourPay);


const computersElement = document.getElementById("computers");
const screenElement = document.getElementById("screen");
const keyboardElement = document.getElementById("keyboard");
const selectedLaptopElement = document.getElementById("selected-laptops");
const descriptionElement = document.getElementById("description");
const priceElement = document.getElementById("price");
const buyElement = document.getElementById("buy-btn");
const imgElement = document.getElementById("img");
let currentComputerPrice = 0;
let computers = [];
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToList(computers));
const addComputersToList = (computers) => {
  computers.forEach(x => addComputerToList(x));
  screenElement.innerText = computers[0].specs[0];
  keyboardElement.innerText = computers[0].specs[1];
  selectedLaptopElement.innerText = computers[0].title;
  descriptionElement.innerText = computers[0].description;
  priceElement.innerText = currencyExchange(computers[0].price);
  currentComputerPrice = computers[0].price;
  imgElement.src = "https://noroff-komputer-store-api.herokuapp.com/"+(computers[0].image);
}

const addComputerToList = (computer) => {
  const computerElement = document.createElement("option")
  computerElement.value = computer.id;
  computerElement.appendChild(document.createTextNode(computer.title));
  computersElement.appendChild(computerElement);
}
const handleComputerChange = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  screenElement.innerText = selectedComputer.specs[0];
  keyboardElement.innerText = selectedComputer.specs[1];
  selectedLaptopElement.innerText = selectedComputer.title;
  descriptionElement.innerText = selectedComputer.description;
  priceElement.innerText = currencyExchange(selectedComputer.price);
  currentComputerPrice = selectedComputer.price;
  imgElement.src = "https://noroff-komputer-store-api.herokuapp.com/"+(selectedComputer.image);

}
computersElement.addEventListener("change", handleComputerChange);

const buyComputer = (e) => {
  console.log(currentComputerPrice);
  if (currentBalance >= currentComputerPrice) {
    currentBalance = currentBalance - parseInt(currentComputerPrice);
    alert("Computer is your now.")
    return document.getElementById("balance").innerHTML = currencyExchange(currentBalance);
  }else{
    alert("You don't have enough money to buy this computer.");
  }
}

buyElement.addEventListener("click",buyComputer);
